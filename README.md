# Blog Site - JavaScript
This repository holds the sample source code for a basic JavaScript implementation of a blog site powered by Oracle Content and Experience.

Please see the complete tutorial at:
[https://www.oracle.com/pls/topic/lookup?ctx=cloud&id=oce-javascript-blog-sample](https://www.oracle.com/pls/topic/lookup?ctx=cloud&id=oce-javascript-blog-sample)

A live version of this project is available at:
[https://headless.mycontentdemo.com/samples/oce-javascript-blog-sample](https://headless.mycontentdemo.com/samples/oce-javascript-blog-sample)


## Running the project
> **Note**  
The __starter__ folder is for developers following instructions in the tutorial where some code must be modified before it can be run.  The __completed__ folder has code that is ready to run.

To build this project:
> cd completed  
> npm install

You can then host the contents of the *src* folder on any web server.

To run using the embedded server code:
> npm start

and then open [http://localhost:8881/oce-javascript-blog-sample/index.html](http://localhost:8881/oce-javascript-blog-sample/index.html)

## Images
Sample images may be downloaded from [https://www.oracle.com/middleware/technologies/content-experience-downloads.html](https://www.oracle.com/middleware/technologies/content-experience-downloads.html) under a separate license.  These images are provided for reference purposes only and may not hosted or redistributed by you.

## Limitations
This sample will not work on IE11 without adding polyfills for ES6 features.

## License
Copyright (c) 2020 Oracle and/or its affiliates and released under the 
[Universal Permissive License (UPL)](https://oss.oracle.com/licenses/upl/), Version 1.0
